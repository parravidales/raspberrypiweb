![logo][]


# Blue Pixel
Bienvenido a la web de **Blue Pixel**. Este servidor cuenta con un servicio de IRC utilizado para obtener información del estado del sistema.

## Agentes disponibles
Los agentes disponibles para atender las peticiones son los siguientes:

+ Agente **Temperatura**: Informa sobre la temperatura del servidor en el momento.

---
*[IRC]: Internet Relay Chat

[logo]: ../img/bluepixel.png