var http = require('http');
//var fs = require('path'); // on v6
var fs = require('fs');
var url = require('url');
var path = require('path');
var raiz = '/var/www';
runner = require("child_process");
//var index = fs.readFileSync(raiz + '/login.html');

function startServer() {
	function onRequest(req, res) { 
		// Send HTML headers and message
		//console.log('Recibida petición de: ' + req);
		res.writeHead(200,{ 'Content-Type': 'text/html' }); 
		res.end(index.html);
	};
	http.createServer(onRequest).listen(80, function(){
   		console.log('Servidor listo en ' + this.address().port);
	});
};

function startBigServer() {
	function onRequest(req, res) {
		var contentType = 'text/html';
		var page = raiz + ((req.url == '/')?'/index.html':req.url);
		console.log('Petición de: ' + page);
		var extname = path.extname(page);
		switch(extname) {
			case '.css':
				contentType = 'text/css';
				break;
			case '.js':
				contentType = 'application/javascript';
				break;
			case '.php':
				//page = raiz + '/php' + req.url;
				contentType = 'text/php';
				console.log('Ejecutando código PHP para ' + page + '...');
				break;
			case '.jpg':
				contentType = 'image/jpeg';
				break;
		}
		console.log('Fichero a enviar de tipo: ' + contentType);
		res.writeHead(200, {'content-type': contentType});

		fs.exists(page, function(existe){
			if(existe){
				fs.readFile(page, function(error, contenido){
					if(error) {
						res.writeHead(500);
						console.log('Error 500: El fichero ' + page + ' no existe');
						res.end();
					} else {
						if(extname != '.php') {
							res.writeHead(200, {'content-type': contentType});
							console.log('Enviando fichero ' + page);
							res.write(contenido);
							res.end();
						} else {
							/*res.writeHead(200, {'content-type': contentType});
							
							runner('php ' + page, function(error, stdout, stderr) { 
								console.log('Enviando fichero ' + page);
								res.write(stdout);
								res.end();
							});*/
							//console.log(req.body.user);
							runner.exec('php ' + page, function(error, stdout, stderr) {
								if(error == 'null') {
									res.writeHead(404);
									res.end('Error en ' + page);
								} else {
									console.log('stdout: ' + stdout);
									console.log('stderr: ' + stderr);
									res.write(stdout);
									res.end();
								}
							});
						}
					}
				});
			} else {
				res.writeHead(404);
				console.log('Error 404: El fichero ' + page + ' no existe');
				res.end();
			}
		});

		//console.log('Petición HTTP respondida');
	}
	http.createServer(onRequest).listen(80, function(){
   		console.log('Servidor listo en ' + this.address().port);
	});
};

startBigServer();
//startServer();

exports.startServer = startServer;
exports.startBigServer = startBigServer;

/*

CONSULTAR
=========

http://stackoverflow.com/questions/12134554/node-js-external-js-and-css-fiels-just-using-node-js-not-express
http://stackoverflow.com/questions/6084360/node-js-as-a-simple-web-server
http://www.davidgranado.com/2011/01/serve-a-static-html-file-with-nodejs/
http://stackoverflow.com/questions/4720343/loading-basic-html-in-nodejs
http://www.aprendiendonodejs.com/
http://www.youtube.com/watch?v=FqSokw58eSM
http://www.google.com/cse?cx=002683415331144861350%3Atsq8didf9x0&q=servir+css+node.js&ie=UTF-8&sa=Search#gsc.tab=0&gsc.q=servir%20css%20node.js&gsc.page=1

*/
