$(function () {
    
    $('#temperature').highcharts({
    
        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        
        title: {
            text: ''
        },
        
        pane: {
            startAngle: -135,
            endAngle: 135,
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },
           
        // the value axis
        yAxis: {
            min: 0,
            max: 80,
            
            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',
    
            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 2,
                rotation: 'auto'
            },
            title: {
                text: 'ºC'
            },
            plotBands: [{
                from: 0,
                to: 50,
                color: '#FFFFFF' // green
            }, {
                from: 50,
                to: 70,
                color: '#DDDF0D' // yellow
            }, {
                from: 70,
                to: 80,
                color: '#DF5353' // red
            }]
        },
    
        series: [{
            name: 'Temperature',
            data: [0],
            tooltip: {
                valueSuffix: ' ºC'
            }
        }]
    
    },
    // Add some life
    function (chart) {

        if (!chart.renderer.forExport) {
            setInterval(function () {
                $.ajax({
                    url: "php/temp.php",
                    dataType: "text",
                    success: function(result) {
                        if(result < 0 || result > 80) {
                            result = 0;
                        }
                        var point = chart.series[0].points[0],newVal;
                        point.update(Math.round(result));
                    }
                });
                
            }, 1000);
        }
    });
});
