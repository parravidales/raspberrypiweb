<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Raspberry Pi</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Le styles -->
	<link href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css" rel="stylesheet">
	<!--<style type="text/css">
        body { 
        	padding-top: 60px;
        	padding-bottom: 40px;
        	background-color: #000000;
        }
    </style>-->
	<link href="css/index.css" rel="stylesheet" media="screen" type="text/css">
	<link href="http://twitter.github.com/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="http://twitter.github.com/bootstrap/assets/images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="http://twitter.github.com/bootstrap/assets/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="http://twitter.github.com/bootstrap/assets/images/apple-touch-icon-114x114.png">
</head>
<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand">
					<i class="icon-stop"></i> Blue pixel<!-- icon-white -->
				</a>
				<div class="nav-collapse">
					<ul class="nav">
						<li><a href="javascript:void(0)">Inicio</a></li>
						<li><a href="http://localhost:9091">Transmission</a></li>
					</ul>
					<ul class="nav pull-right">
						<li class="dropdown">
							<a id="drop1" class="dropdown-toggle" data-toggle="dropdown"
								data-target="#">
									<?php
										if($_SESSION['nickname']) {
											echo $_SESSION['nickname'];
										} else { echo 'Guest';}
									?>
								<b class="caret"></b>
							</a>
                        	<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        		<a onclick="javascript:mostrarLogin()">
		                        	<i class="icon-stop icon-off"></i> 
		                        	Desconectar
		                        </a>
                        	</ul>
						</li>
                        <li></li><!--data-toggle="modal" href="#example"-->
                    </ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>

	<div class="container">
		<!-- Main hero unit for a primary marketing message or call to action -->
		<div class="hero-unit">
			<h1><img src="img/bluepixel.png">  Blue pixel</h1>
			
			<p></p>
			<!--<p><a class="btn btn-primary btn-large">Learn more &raquo;</a></p>-->
		</div>
		<!-- Example row of columns -->
		<div class="row">
			<div class="span12">
				<div class="tabbable tabs-left">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#1" data-toggle="tab">Inicio</a></li>
						<li><a href="#2" data-toggle="tab">Transmission</a></li>
						<li><a href="#3" data-toggle="tab">Perfil</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="1">
							<p>I'm in Section 1.</p>
						</div>
						<div class="tab-pane" id="2">
							<p>En construcción</p>
							<div class="progress progress-striped progress-warning active" style="width: 50%;">
								<div class="bar" style="width: 40%;"></div>
							</div>
						</div>
						<div class="tab-pane" id="3">
							<p>Yeah, I'm in Section 3.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

	  	<hr>

		<footer style="text-align:center">
			<p>Blue Pixel - &copy; Parra 2013</p>
		</footer>
	</div> <!-- /container -->

	<!-- Le javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="http://code.jquery.com/jquery.min.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-transition.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-alert.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-modal.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-dropdown.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-scrollspy.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-tab.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-tooltip.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-popover.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-button.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-collapse.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-carousel.js"></script>
	<script src="https://raw.github.com/twitter/bootstrap/master/js/bootstrap-typeahead.js"></script>
	<script src="js/main.js"></script>
</body>
</html>