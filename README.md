# Raspberry Pi web

## Introducción

Este *README* contiene la información del contenido de la carpeta _www_.
Dicha carpeta contiene todos los ficheros necesarios para la visualización de la web, así como de los javascript que permiten
la ejecución de código que dota de dinamismo a la web.

Por otra parte, también se encuentra disponible el _Javascript_ que contiene
el servidor de *NodeJS* que gestiona las peticiones de los clientes, sirviendo
las páginas solicitadas y ejecutando porciones de código, sobre todo escritas en _PHP_.

## Contenido de la carpeta _www_:

+ **css**-\>    Contiene las hojas de estilo de la página en _CSS3_.
+ **img**-\>    Carpeta de imágenes.
+ **js**-\>     Ficheros _Javascript_ de la página web.
+ **php**-\>    Ficheros _PHP_ de la página web.
+ **server**-\> Contiene el servidor *NodeJS* que atiende las peticiones.
+ **v2**-\>     Versión _alpha_ de la siguiente versión.
+ **video**-\>  Contiene las imágenes usadas en la defensa del PFC de II.
+ **.**-\>      Ficheros _HTML_, _PHP_ y _favicon_ de la página web.
